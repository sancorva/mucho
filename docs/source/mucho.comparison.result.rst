mucho.comparison.result package
===============================

Submodules
----------

mucho.comparison.result.result module
-------------------------------------

.. automodule:: mucho.comparison.result.result
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: mucho.comparison.result
    :members:
    :undoc-members:
    :show-inheritance:
