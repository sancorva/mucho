Rule Evaluation Context
===============================

The :ref:`quick-start-basic-example` of the quick start guide shows how the
virtual machine needs some context to evaluate the rules:

.. code-block:: python
   :emphasize-lines: 18-20

   from mucho.comparison import ComparisonResult
   from mucho.dsl.compiler import Compiler
   from mucho.dsl.virtualmachine import VirtualMachine

   rules_text = """
   discarded: Those whose titles are very different
   titles.similarity <= 50
   => mismatch

   perfect: Those matching almost perfectly
   titles.similarity >= 90 and years.difference < 2
   => match
   """

   compiler = Compiler()
   rules = compiler.compile(rules_text, debug=True)

   context = ComparisonResult(
       titles=ComparisionResult(similarity=95),
       years=ComparisonResult(difference=1))

   virtual_machine = VirtualMachine()
   satisfied_rule = virtual_machine.run(rules, context)
   if satisfied_rule:
       print("Rule '{0}' was satisfied with result: {1}".format(
           satisfied_rule.id, satisfied_rule.result.value))
   else:
       print("No rule was satisfied")

This context provides the values of the variables used in the rules, e.g. it
sets :code:`titles.similarity` value to :code:`95`.

Although the context is modeled with the class
:py:class:`mucho.comparison.result.result.ComparisonResult`, the method
:py:func:`mucho.dsl.virtualmachine.virtualmachine.VirtualMachine.run` will
accept as a valid context any object that can be accessed via the dot notation.

Returning to the example, a way to specify the value of the
:code:`titles.similarity` variable is defining it in a :code:`ComparisonResult`
object:

.. code-block:: python
   :emphasize-lines: 2

   context = ComparisonResult(
       titles=ComparisionResult(similarity=95),
       years=ComparisonResult(difference=1))
