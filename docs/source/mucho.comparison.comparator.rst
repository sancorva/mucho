mucho.comparison.comparator package
===================================

Subpackages
-----------

.. toctree::

    mucho.comparison.comparator.dimension
    mucho.comparison.comparator.entity

Module contents
---------------

.. automodule:: mucho.comparison.comparator
    :members:
    :undoc-members:
    :show-inheritance:
