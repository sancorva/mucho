mucho.comparison package
========================

Subpackages
-----------

.. toctree::

    mucho.comparison.comparator
    mucho.comparison.result

Module contents
---------------

.. automodule:: mucho.comparison
    :members:
    :undoc-members:
    :show-inheritance:
