SHELL := /bin/bash

check: lint mypy test

test:
	@pipenv run tox -v

test-coverage:
	@pipenv run coverage run --source mucho/ -m unittest -v
	@pipenv run coverage report --show-missing

lint:
	@pipenv run pycodestyle .

lint-strict:
	@pipenv run pylint mucho

mypy:
	@pipenv run mypy --config-file mypy.ini --verbose mucho

dist:
	@pipenv run python setup.py sdist bdist_wheel
	@pipenv run python -m twine upload dist/*
