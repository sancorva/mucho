"""
mucho DSL grammar definition in Lark format and classes in charge of the
compilation process of the rules written using the mucho DSL.
"""

from .compiler import Compiler, CompilationError
